import java.util.ArrayDeque;
import java.util.PriorityQueue;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.PriorityBlockingQueue;

public class queue_iterator {
  public static void main(String[] argv) {
    /* BUG: Sequence of arrayDeque.iterator, arrayDeque.remove(-1), iterator.next
       produces wrong type of Exception. */
    ArrayDeque<Integer> testArrayDeque = new ArrayDeque<Integer>();
    Iterator<Integer> it = testArrayDeque.iterator();
    try {
      testArrayDeque.remove(-1);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (NoSuchElementException e) {
    }

    /* Correct result with remove(0) */
    testArrayDeque = new ArrayDeque<Integer>();
    it = testArrayDeque.iterator();
    try {
      testArrayDeque.remove(0);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (NoSuchElementException e) {
    }

    /* Correct result with remove(-1) on PriorityQueue */
    PriorityQueue<Integer> testPriorityQueue = new PriorityQueue<Integer>();
    it = testPriorityQueue.iterator();
    try {
      testPriorityQueue.remove(-1);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (NoSuchElementException e) {
    }

    /* Correct result with remove(-1) on LinkedBlockingDeque */
    LinkedBlockingDeque<Integer> testLinkedBlockingDeque =
      new LinkedBlockingDeque<Integer>();
    it = testLinkedBlockingDeque.iterator();
    try {
      testLinkedBlockingDeque.remove(-1);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (NoSuchElementException e) {
    }

    /* Correct result with remove(-1) on LinkedBlockingQueue */
    LinkedBlockingQueue<Integer> testLinkedBlockingQueue =
      new LinkedBlockingQueue<Integer>();
    it = testLinkedBlockingQueue.iterator();
    try {
      testLinkedBlockingQueue.remove(-1);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (NoSuchElementException e) {
    }

    /* Correct result with remove(-1) on LinkedTransferQueue */
    LinkedTransferQueue<Integer> testLinkedTransferQueue =
      new LinkedTransferQueue<Integer>();
    it = testLinkedTransferQueue.iterator();
    try {
      testLinkedTransferQueue.remove(-1);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (NoSuchElementException e) {
    }

    /* Correct result with remove(-1) on ArrayBlockingQueue */
    ArrayBlockingQueue<Integer> testArrayBlockingQueue =
      new ArrayBlockingQueue<Integer>(1);
    it = testArrayBlockingQueue.iterator();
    try {
      testArrayBlockingQueue.remove(-1);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (NoSuchElementException e) {
    }

    /* Correct result with remove(-1) on PriorityBlockingQueue */
    PriorityBlockingQueue<Integer> testPriorityBlockingQueue =
      new PriorityBlockingQueue<Integer>(1);
    it = testPriorityBlockingQueue.iterator();
    try {
      testPriorityBlockingQueue.remove(-1);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (NoSuchElementException e) {
    }
  }
}
