#!/bin/sh
time scala -classpath . modbat.jar \
	-s=7 \
	-n=1000 \
	--abort-probability=0.02 \
	model.SimpleListModel
