import java.util.concurrent.ConcurrentSkipListSet;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class hash_iterator {
  public static void main(String[] argv) {
    /* BUG: Sequence of iterator, remove(-1), iterator.next
       produces wrong type of Exception. */
    ConcurrentSkipListSet<Integer> testConcurrentSkipListSet =
      new ConcurrentSkipListSet<Integer>();
    Iterator<Integer> it = testConcurrentSkipListSet.iterator();
    try {
      testConcurrentSkipListSet.remove(-1);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (NoSuchElementException e) {
    }

    HashSet<Integer> testHashSet = new HashSet<Integer>();
    it = testHashSet.iterator();
    try {
      testHashSet.remove(-1);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (NoSuchElementException e) {
    }

    /* Correct result with remove(0) */
    TreeSet<Integer> testTreeSet = new TreeSet<Integer>();
    it = testTreeSet.iterator();
    try {
      testTreeSet.remove(0);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (NoSuchElementException e) {
    }

    /* Correct result with remove(-1) on LinkedList */
    LinkedHashSet<Integer> testLinkedHashSet = new LinkedHashSet<Integer>();
    it = testLinkedHashSet.iterator();
    try {
      testLinkedHashSet.remove(-1);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (NoSuchElementException e) {
    }
  }
}
