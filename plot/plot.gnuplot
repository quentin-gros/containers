set terminal postscript eps enhanced color "Helvetica" 16
set grid
set y2range [0:140]
set yrange [0:15]
set xlabel "Number of test cases"
set y2label "Time [sec]"
set ylabel "Number of warnings"
set y2tics
set xrange [0:100000]
set key top left
set boxwidth 700
set ytics nomirror
set xtics out ("1000" 1000, "" 2000, 5000, 10000, 20000, 50000, 100000) rotate by 45 right \
nomirror offset 0.1,0.25

set out "plot.ps"

plot \
"precond-llist.data" using 1:3 axes x1y1 with linespoints lw 2 title "LinkedList: Number of warnings", \
"precond-alist.data" using 1:3 axes x1y1 with linespoints lw 2 title "ArrayList: Number of warnings", \
"precond-llist.data" using 1:2 axes x1y2 with linespoints lw 2 title "LinkedList: Test execution time", \
"precond-alist.data" using 1:2 axes x1y2 with linespoints lw 2 title "ArrayList: Test execution time"

set out "nio.ps"
set y2range [0:100]
set yrange [0:9]
plot \
"precond-nio.data" using 1:3 axes x1y1 with linespoints lw 2 title "java.nio: Number of warnings", \
"precond-nio.data" using 1:4 axes x1y1 with linespoints lw 2 title "java.nio: Number of warnings after fix", \
"precond-nio.data" using 1:2 axes x1y2 with linespoints lw 2 title "java.nio: Test execution time"
#"precond-nio.data" using 1:($4/7) axes x1y1 with linespoints lw 2 title "java.nio: Transition coverage"
