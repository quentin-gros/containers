#!/bin/sh

NEWEST="`ls -t | head -1`"

if [ "`echo ${NEWEST} | grep '\.data$'`" != "" -o \
     "`echo ${NEWEST} | grep '^plot\.'`" != "" -o \
     "`ls *.pdf | wc -l | tr -cd '[0-9]'`" == "0" ]
then
# data file or plot file change, replot everything
	gnuplot < plot.gnuplot
	for F in *.ps
	do
		B="`echo $F | sed -e 's/\.ps$//'`"
		ps2epsi $F
		mv $B.eps{i,}
		epstopdf $B.eps
		done
fi
