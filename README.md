# README #

Modbat demo; requires Scala 2.11.X and JDK 1.8.X.

## To compile: ./compile.sh ##

## To run the faulty array list: ./runArrayList.sh ##

Modbat will report one failed test case out of 1,000 tests (with the
given fixed random seed).
It will also report that some preconditions are always true or always
false; in this case, this is because not enough tests are generated to
cover both cases.

To view the error trace for the failed test, run

	./showTrace.sh 6945df2424fcf4ff.err 

## To run the correct linked list: ./runLinkedList.sh ##

Transition coverage can be maxed out by running 10,000 tests;
change `-n=1000` to `-n=10000` in the shell scripts for this.
More tests will create more error traces for ArrayList.

## To run the stand-alone unit tests on various container classes: ##

	cd JUnitTest
	javac *.java
	java iterator_test  # ArrayList and related legacy classes fail
	java hash_iterator  # all OK
	java queue_iterator # all OK
