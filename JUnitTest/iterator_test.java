import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Stack;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArrayList;

public class iterator_test {
  public static void main(String[] argv) {
    /* BUG: Sequence of arrayList.iterator, arrayList.remove(-1), iterator.next
       produces wrong type of Exception. */
    ArrayList<Integer> testArrayList = new ArrayList<Integer>();
    Iterator<Integer> it = testArrayList.iterator();
    try {
      testArrayList.remove(-1);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (ConcurrentModificationException e) {
      System.err.println("ArrayList: Should be NoSuchElementException!");
    }

    /* Correct result with remove(0) */
    testArrayList = new ArrayList<Integer>();
    it = testArrayList.iterator();
    try {
      testArrayList.remove(0);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (NoSuchElementException e) {
    }

    /* Correct result with remove(-1) on LinkedList */
    LinkedList<Integer> testLinkedList = new LinkedList<Integer>();
    it = testLinkedList.iterator();
    try {
      testLinkedList.remove(-1);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (NoSuchElementException e) {
    }

    /* Same bug with remove(-1) on Stack */
    Stack<Integer> testStack = new Stack<Integer>();
    it = testStack.iterator();
    try {
      testStack.remove(-1);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (ConcurrentModificationException e) {
      System.err.println("Stack: Should be NoSuchElementException!");
    }

    /* Correct result with remove(-1) on Vector */
    Vector<Integer> testVector = new Vector<Integer>();
    it = testVector.iterator();
    try {
      testVector.remove(-1);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (ConcurrentModificationException e) {
      System.err.println("Vector: Should be NoSuchElementException!");
    }

    /* Correct result with remove(-1) on CopyOnWriteArrayList */
    CopyOnWriteArrayList<Integer> testCopyOnWriteArrayList =
      new CopyOnWriteArrayList<Integer>();
    it = testCopyOnWriteArrayList.iterator();
    try {
      testCopyOnWriteArrayList.remove(-1);
    } catch (IndexOutOfBoundsException e) {
    }
    try {
      it.next();
    } catch (NoSuchElementException e) {
    }
  }
}
